;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (a-star-tests)
  (export a-star-tests)
  (import (rnrs)
          (testing)
          (a-star))

  (define-specs a-star-specs
    (describe "A* algorithm"
      (define-record-type node
        (nongenerative a-star-specs-node)
        (fields (mutable neighbours)
                (immutable position))
        (protocol (lambda (new)
                    (lambda (pos)
                      (new (list) pos)))))

      (define (node-hash node)
        (node-position node))
      (define (neighbour? node another)
        (or (member node (node-neighbours another))
            (member another (node-neighbours node))))
      (define (make-heuristic goal-node)
        (lambda (node)
          (max 0 (- (node-position goal-node) (node-position node)))))
      (define (distance node-a node-b)
        (assert (neighbour? node-a node-b))
        1)
      (define (node-ids nodes)
        (map node-position nodes))

      (define-syntax define-goal-node
        (syntax-rules ()
          [(_ node goal? heuristics)
           (begin
             (define (goal? n)
               (eq? n node))
             (define heuristics (make-heuristic node)))]))
      
      (context "can find a path"
        (define first (make-node 1))
        (define second (make-node 2))
        (define third (make-node 3))
        (it "in trivial situations - single node only"
          (define-goal-node first goal? heuristic)
          (assert-equals (list first) (A* heuristic distance node-hash eq? node-neighbours first goal?)))
        (it "in tivial situations - just two nodes"
          (define-goal-node second goal? heuristic)
          (node-neighbours-set! first (list second))
          (node-neighbours-set! second (list first))
          (assert-equals (list first second) (A* heuristic distance node-hash eq? node-neighbours first goal?)))
        (it "in tivial situations - three nodes in a row"
          (define-goal-node third goal? heuristic)
          (node-neighbours-set! first (list second))
          (node-neighbours-set! second (list first third))
          (node-neighbours-set! third (list second))
          (assert-equals (list first second third) (A* heuristic distance node-hash eq? node-neighbours first goal?))))
      (context "reports that there is no path"
        (define first (make-node 1))
        (define second (make-node 2))
        (it "when target node is not linked to the starting node"
          (define-goal-node second goal? heuristic)
          (assert-true (not (A* heuristic distance node-hash eq? node-neighbours first goal?)))))
      (context "can find shortest path"
        (define first (make-node 1))
        (define second (make-node 2))
        (define third (make-node 3))
        (define fourth (make-node 4))
        (it "in situation when there are two paths the shortes (by heuristics) will be taken"
          (define-goal-node fourth goal? heuristic)
          ;; There is a rhombus-type graph.
          (node-neighbours-set! first (list second third))
          (node-neighbours-set! second (list first fourth))
          (node-neighbours-set! third (list first fourth))
          (node-neighbours-set! fourth (list second third))
          (assert-equals (node-ids (list first third fourth)) (node-ids (A* heuristic distance node-hash eq? node-neighbours first goal?)))))))

  (define (a-star-tests)
    (run-tests (a-star-specs))))

(top-level-program (import (a-star-tests)
                           (testing)
                           (only (testing output) colored-output)
                           (srfi :39)
                           (rnrs))
  (parameterize ([colored-output #t])
    (output-test-summary (a-star-tests) (current-output-port)))
  (exit))
