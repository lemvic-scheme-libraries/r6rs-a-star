# R6RS A* implementation

## Overview

This is a simple unoptimized implementation of an A* (extended Dijkstra's) shortest path finding algorithm.

## API

There is only one function exported.

### `(A* heuristic-func distance-func node-hash node-equals? neighbours-func start-node goal?)`

This function find a shortest path between `start-node` and `goal-node`. Arguments are:

- `heuristic-func` - will be given a node and has to return estimate of the distance between the node and potential goal node
- `distance-func` - will be given 2 nodes and has to return the distance between them
- `node-hash` - function to compute hash value of a node of the path
- `node-equals?` - predicate to compare nodes for equality
- `neighbours-func` - function that should return neighbours of the given node
- `start-node` - start of the search
- `goal?` - predicate used to detect if algorithm has reached the goal.

Returns the list of nodes to traverse in order to reach `goal-node` from `start-node` or `#f` if there is no path connecting those.
