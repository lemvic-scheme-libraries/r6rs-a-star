;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (a-star)
  (export A*)
  (import (rnrs)
          (srfi :26)
          (binary-heap))

  ;; Finds a path between start-node and the first node that matches `goal?` predicate (if it exists) using
  ;; heuristic-func to estimate the distance from a given node to the goal node (it should be consistent with `goal?`)
  ;; and neighbours-func to determine neighbours of a given node.
  ;; node-hash and node-equals? are required to work with the node types.
  (define (A* heuristic-func distance-func node-hash node-equals? neighbours-func start-node goal?)
    (let ([came-from (make-hashtable node-hash node-equals?)]
          [path-costs (make-hashtable node-hash node-equals?)]
          [path-estimates (make-hashtable node-hash node-equals?)])

      (define (reconstruct-path current)
        (let loop ([result (list current)]
                   [next (hashtable-ref came-from current #f)])
          (cond [next (loop (cons next result) (hashtable-ref came-from next #f))]
                [else result])))
      (define (path-cost node)
        (hashtable-ref path-costs node +inf.0))
      (define (path-estimate node)
        (hashtable-ref path-estimates node +inf.0))
      (define (update-came-from! node parent)
        (hashtable-set! came-from node parent))
      (define (update-path-cost! node cost)
        (hashtable-set! path-costs node cost))
      (define (update-path-estimate! node estimate)
        (hashtable-set! path-estimates node estimate))
      (define (node<? node-a node-b)
        (< (path-estimate node-a) (path-estimate node-b)))
      (define (already-seen? node)
        (and (hashtable-ref came-from node #f) #t))
      (define (process-neighbour! open-set current current-cost neighbour)
        (let* ([neighbour-cost (+ current-cost (distance-func current neighbour))]
               [neighbour-estimate (+ neighbour-cost (heuristic-func neighbour))]
               [already-seen (already-seen? neighbour)])
          (when (< neighbour-cost (path-cost neighbour))
            (update-path-cost! neighbour neighbour-cost)
            (update-path-estimate! neighbour neighbour-estimate)
            (update-came-from! neighbour current)
            (if already-seen
                (heap-update! open-set (cut node-equals? neighbour <>))
                (heap-push! open-set neighbour)))))

      (let ([open-set (make-binary-heap node<? start-node)])
        (update-path-cost! start-node 0)
        (update-path-estimate! start-node (heuristic-func start-node))
        (let loop ([current (heap-pop! open-set)])
          (cond [(goal? current) (reconstruct-path current)]
                [else (let ([neighbours (neighbours-func current)]
                            [current-cost (path-cost current)])
                        (for-each (cut process-neighbour! open-set current current-cost <>) neighbours)
                        (if (heap-empty? open-set)
                            #f
                            (loop (heap-pop! open-set))))]))))))
